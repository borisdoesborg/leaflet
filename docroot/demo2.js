// Leaflet kaart in de 28992 projectie. Zie
// https://geoforum.nl/t/wmts-in-28992-met-leaflet-kaartje/5377/4

var RD = new L.Proj.CRS(
    'EPSG:28992','+proj=sterea +lat_0=52.15616055555555 +lon_0=5.38763888888889 +k=0.9999079 +x_0=155000 +y_0=463000 +ellps=bessel +units=m +towgs84=565.2369,50.0087,465.658,-0.406857330322398,0.350732676542563,-1.8703473836068,4.0812 +no_defs', {
        origin: [-285401.920, 903401.920],
        resolutions: [3440.640, 1720.320, 860.160, 430.080, 215.040, 107.520, 53.760, 26.880, 13.440, 6.720, 3.360, 1.680, 0.840, 0.420, 0.210, 0.105],
        bounds: L.bounds([-285401.920, 903401.920], [595401.920, 22598.080])
    });

// brtachtergrondkaart layer van nationaalgeoregister in projectie 28992
url = 'https://geodata.nationaalgeoregister.nl/tiles/service/wmts?&service=WMTS&request=GetTile&version=1.0.0&layer=brtachtergrondkaart&style=default&tilematrixset=EPSG%3A28992&format=image%2Fpng&height=256&width=256&tilematrix={z}&tilecol={x}&tilerow={y}'
brtachtergrondkaart = new L.TileLayer( url ,
    {
        layer: 'brtachtergrondkaart',
        style: "default",
        format: "image/png",
    }
);

// gemeenten layer van nationaalgeoregister in zelfde projectie (automatisch)
var gemeenten = L.tileLayer.wms("https://geodata.nationaalgeoregister.nl/bestuurlijkegrenzen/wms?", {
        layers: 'gemeenten',
        format: 'image/png',
        transparent: true,
    }
);

// Start de leaflet kaart met de projectie 28992 en de gedefinieerde layers.
var map = new L.Map('mapid', {
    crs: RD,
    layers: [brtachtergrondkaart, gemeenten]
});

var center = L.point(160000.0, 480803.840);
map.setView(RD.projection.unproject(center), 5);
