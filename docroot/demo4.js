var mymap = L.map('mapid');
mymap.setView([52.6, 4.8], 11);

// brtachtergrondkaart layer van nationaalgeoregister in projectie 3857
L.tileLayer('https://geodata.nationaalgeoregister.nl/tiles/service/wmts/brtachtergrondkaart/EPSG:3857/{z}/{x}/{y}.png', {
    maxZoom: 18,
    minZoom: 8,
    tileSize: 256,
    zoomOffset: 0
}).addTo(mymap);

// gemeenten layer van nationaalgeoregister in projectie 3857
var gemeenten = L.tileLayer.wms("https://t-rigo-plancapaciteit.finalist.nl/geoserver/plancapaciteit_public/wms?", {
        layers: 'capaciteit_public:municipalities',
        format: 'image/png',
        transparent: true,
    }
);
gemeenten.addTo(mymap);

// gemeenten layer van nationaalgeoregister in projectie 3857
var plans = L.tileLayer.wms("https://t-rigo-plancapaciteit.finalist.nl/geoserver/plancapaciteit_public/wms?", {
        layers: 'plancapaciteit_public:plans_public',
        format: 'image/png',
        transparent: true,
    }
);
plans.addTo(mymap);

