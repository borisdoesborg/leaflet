var mymap = L.map('mapid');
mymap.setView([52.6, 4.8], 11);

// brtachtergrondkaart layer van nationaalgeoregister in projectie 3857
L.tileLayer('https://geodata.nationaalgeoregister.nl/tiles/service/wmts/brtachtergrondkaart/EPSG:3857/{z}/{x}/{y}.png', {
    maxZoom: 18,
    minZoom: 8,
    tileSize: 256,
    zoomOffset: 0
}).addTo(mymap);

// gemeenten layer van nationaalgeoregister in projectie 3857
var gemeenten = L.tileLayer.wms("https://geodata.nationaalgeoregister.nl/bestuurlijkegrenzen/wms?", {
        layers: 'gemeenten',
        format: 'image/png',
        transparent: true,
    }
);
gemeenten.addTo(mymap);


